package requests

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

var availableMethods = []string{"GET", "POST", "PUT", "PATCH", "DELETE"}

const httpClientTimeout = time.Second * 60

type HttpRequestResult struct {
	Body       string
	Headers    map[string]string
	Error      string
	StatusCode int
}

type httpClientResult struct {
	Body       []byte
	Headers    map[string]string
	Error      error
	StatusCode int
}

func MakeRequest(httpRequest Request) HttpRequestResult {
	var respError = ""
	var body = ""
	var headers = map[string]string{}
	var statusCode = 0
	if isAvailableMethod(httpRequest.Method) {
		res := clientCall(httpRequest)
		if res.Error != nil {
			respError = fmt.Sprintf("%s", res.Error)
		}
		body = fmt.Sprintf("%s", res.Body)
		headers = res.Headers
		statusCode = res.StatusCode
	} else {
		respError = fmt.Sprintf("undefined http request method")
	}
	return HttpRequestResult{Body: body, Headers: headers, Error: respError, StatusCode: statusCode}
}

func isAvailableMethod(method string) bool {
	for _, m := range availableMethods {
		if m == method {
			return true
		}
	}
	return false
}

func clientCall(httpRequest Request) httpClientResult {
	var client = http.Client{Timeout: httpClientTimeout}
	var bodyStr = []byte(httpRequest.Body)
	var body = []byte{}
	var statusCode = 0
	req, _ := http.NewRequest(httpRequest.Method, httpRequest.Url, bytes.NewBuffer(bodyStr))
	for key, value := range httpRequest.Headers {
		req.Header.Set(key, value)
	}
	resHeaders := map[string]string{}
	res, err := client.Do(req)
	if res != nil {
		statusCode = res.StatusCode
		defer res.Body.Close()
		body, _ = ioutil.ReadAll(res.Body)
		for k, v := range res.Header {
			resHeaders[k] = v[0]
		}
	} else {
		if err == nil {
			err = errors.New("Daemon description: Empty response")
		}
	}
	return httpClientResult{Body: body, Error: err, Headers: resHeaders, StatusCode: statusCode}
}
