package requests

import (
	"fmt"
	"net/http"
	"request_daemon/responses"
	"runtime"
)

var jobId int64 = 0

type Request struct {
	Id      int64
	Method  string
	Url     string
	Body    string
	Headers map[string]string
}

func Add(requestsList []Request) string {
	jobId++
	setSize := len(requestsList)
	strJobId := fmt.Sprintf("%v", jobId)
	responses.InitSet(strJobId, setSize)
	for _, request := range requestsList {
		go makeFullRequest(request, strJobId)
	}
	return strJobId
}

func makeFullRequest(request Request, jobId string) {
	result := MakeRequest(request)
	strId := fmt.Sprintf("%v", request.Id)
	if result.Error != "" {
		str_error := fmt.Sprintf("%v", result.Error)
		response := responses.Response{Id: strId, Error: str_error}
		responses.Add(jobId, response)
	} else {
		if result.StatusCode == http.StatusOK {
			response := responses.Response{Id: strId, Body: result.Body, Headers: result.Headers}
			responses.Add(jobId, response)
		}
	}
}

func QueueCount() int {
	return runtime.NumGoroutine() - 5
}
