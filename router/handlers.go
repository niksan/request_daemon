package router

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"request_daemon/requests"
	"request_daemon/responses"
)

func indexHandler(c *gin.Context) {
	c.String(http.StatusOK, "Hello world!")
}

func addHandler(c *gin.Context) {
	list := c.PostForm("list")
	var requestsList []requests.Request
	json.Unmarshal([]byte(list), &requestsList)
	output := requests.Add(requestsList)
	c.String(http.StatusOK, output)
}

func queueHandler(c *gin.Context) {
	count := requests.QueueCount()
	res := fmt.Sprintf("%s", count)
	c.String(http.StatusOK, res)
}

func responseHandler(c *gin.Context) {
	responseSetId := c.Param("id")
	result := responses.GetResponseSet(responseSetId)
	if result.Id == "" {
		errorStr := "Has no data"
		result = responses.ResponsesSet{Error: errorStr}
	}
	c.PureJSON(http.StatusOK, result)
}
