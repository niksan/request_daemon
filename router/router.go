package router

import "github.com/gin-gonic/gin"

func New() *gin.Engine {
	r := gin.Default()
	r.GET("/", indexHandler)
	r.POST("/add/", addHandler)
	r.GET("/queue/", queueHandler)
	r.GET("/responses/:id/", responseHandler)
	return r
}
