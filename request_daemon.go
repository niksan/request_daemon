package main

import (
	"request_daemon/responses"
	"request_daemon/router"
)

func main() {
	responses.CleanLoop()
	r := router.New()
	r.Run(":8080")
}
