package responses

import (
	"time"
)

const loopSleepTime = 30 * time.Second
const maxCompletedAge = 30 * time.Minute
const maxCreatedAtAge = 50 * time.Minute

func responsesSetsToClean() []int {
	res := []int{}
	for i, set := range ResponsesList {
		if time.Since(set.CompletedAt) > maxCompletedAge || time.Since(set.CreatedAt) > maxCreatedAtAge {
			res = append(res, i)
		}
	}
	return res
}

func CleanLoop() {
	go func() {
		for {
			indexes := responsesSetsToClean()
			for iteration, index := range indexes {
				real_index := index - iteration
				ResponsesList = append(ResponsesList[:real_index], ResponsesList[real_index+1:]...)
			}
			time.Sleep(loopSleepTime)
		}
	}()
}
