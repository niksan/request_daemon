package responses

import (
	"time"
)

type Response struct {
	Id      string            `json:"id"`
	Body    string            `json:"body"`
	Headers map[string]string `json:"headers"`
	Error   string            `json:"error"`
}

type ResponsesSet struct {
	Id          string     `json:"id"`
	List        []Response `json:"list"`
	Size        int        `json:"size"`
	CreatedAt   time.Time  `json:"created_at"`
	CompletedAt time.Time  `json:"completed_at"`
	Error       string
}

var ResponsesList []ResponsesSet

func InitSet(jobSetId string, setSize int) {
	createdAt := time.Now()
	emptyList := []Response{}
	setInstance := ResponsesSet{Id: jobSetId, List: emptyList, Size: setSize, CreatedAt: createdAt}
	ResponsesList = append(ResponsesList, setInstance)
}

func Add(parentId string, response Response) {
	for index, set := range ResponsesList {
		if set.Id == parentId {
			set.List = append(set.List, response)
			if len(set.List) == set.Size {
				set.CompletedAt = time.Now()
			}
			ResponsesList[index] = set
			return
		}
	}
}

func GetResponseSet(id string) ResponsesSet {
	for _, set := range ResponsesList {
		if set.Id == id {
			return set
		}
	}
	return ResponsesSet{}
}
